# README #

### Què és aquest repositori? ###
Aquest repositori és el codi font de l'aplicació de CPL.

### Com ho començo tot? ###
1. Instal·lar react-native
2. Descarregar el repositori (és un projecte react-native)
3. Instal·lar els node_modules
4. Executa (react-native run...) i et dirà quins mòduls falten per instal·lar (com sqlite i d'altres que els he instal·lat externament). Instal·la'ls
5. Ja ho tens! (segurament no.. i s'ha d'anar perfilant coses fins que funcioni)

Pels testos només cal posar a "true" els camps que hi ha al constructor de HomeScreenController.js (pel test de l'state necessitaràs canviar el path a testAdapter.js per guardar els arxius.

### Guia de contribucions ###
Actualment deixo el codi obert a tothom i si hi ha algú interessat a col·laborar en el projecte que m'ho faci saber!

### Qui sóc? ###
Pau Sabé, desenvolupador de Software. Creador d'aquesta App.
També col·labora en el projecte Alex Ruiz.

### Contacte ###
Qualsevol dubte contacta amb mi: info@pausabe.com